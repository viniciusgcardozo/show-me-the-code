# ShowMeTheCode

This project was made for CI&T technical test, for Javascript Developer oportunity.

## Running app

Run `ng serve` or `npm run start` for up the projectt to `http://localhost:4200/` with live reload.

## Running unit tests

Run `ng test` or `npm run test` to execute the unit tests via [Karma](https://karma-runner.github.io).
