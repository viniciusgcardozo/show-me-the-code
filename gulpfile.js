'use strict';
 
var gulp = require('gulp');
var sass = require('gulp-sass');
 
gulp.task( 'default', [ 'sass' ] )

gulp.task('sass', function () {
    return gulp.src('./src/app/**/*.scss')
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(gulp.dest('./src/app'));
});
 
gulp.task('sass:watch', function () {
    gulp.watch('./src/app/**/*.scss', ['sass']);
});