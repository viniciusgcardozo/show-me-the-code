import { TestBed, inject } from '@angular/core/testing';

import { HttpService } from './http.service';
import { Http } from '@angular/http';

describe('HttpService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        HttpService,
        {provide: Http, useValue: MockHttp}
      ]
    });
  });

  it('should be created', inject([HttpService], (service: HttpService) => {
    expect(service).toBeTruthy();
  }));
});

class MockHttp {}
