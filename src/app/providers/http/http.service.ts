import { Injectable } from '@angular/core';
import { HttpModule } from '@angular/http';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import { Constants } from '../../utils/constants';

@Injectable()
export class HttpService {

  constructor(public http: Http) { }

  private genericGet(url): Promise<any> {
    return this.http
      .get(url)
      .toPromise()
      .then(res => res.json().data);
  }
  public locationAndPricingList(): Promise<any> {
    return this.genericGet(`${Constants.SERVER}${Constants.ROUTES.PRICING}`);
  }
  public locationDetailList(): Promise<any> {
    return this.genericGet(`${Constants.SERVER}${Constants.ROUTES.DETAILS}`);
  }
  public planDescriptionList(): Promise<any> {
    return this.genericGet(`${Constants.SERVER}${Constants.ROUTES.PLANS}`);
  }
}
