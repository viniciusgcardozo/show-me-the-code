export class Constants {
    static SERVER = 'https://afternoon-wave-67876.herokuapp.com';
    static ROUTES = {
        PRICING: '/ddd/pricing',
        DETAILS: '/ddd/details',
        PLANS: '/plans'
    };
}
