import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { HttpService } from './providers/http/http.service';
import { DddInputComponent } from './components/ddd-input/ddd-input.component';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { PlanOutputComponent } from './components/plan-output/plan-output.component';
import { PlanCalculatorService } from './components/plan-output/services/plan-calculator.service';
describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        CommonModule,
        FormsModule
      ],
      declarations: [
        AppComponent,
        DddInputComponent,
        PlanOutputComponent
      ],
      providers: [
        {provide: HttpService, useValue: MockHttpService},
        {provide: PlanCalculatorService, useValue: MockPlanCalculatorService}
      ]
    }).compileComponents();
  }));
  it('should be created', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const component = fixture.componentInstance;
    expect(component).toBeTruthy();
  }));

});

class MockHttpService {
  static planDescriptionList() {
    return new Promise<any>((resolve, reject) => {
      resolve([
        {plan: 'FaleMais 30', time: '30'},
        {plan: 'FaleMais 60', time: '60'},
        {plan: 'FaleMais 120', time: '120'}
      ]);
    });
  }
  static locationDetailList() {
    return new Promise<any>((resolve, reject) => {
      resolve([
        {ddd: '011', city: 'São Paulo'},
        {ddd: '016', city: 'Ribeirão Preto'},
        {ddd: '017', city: 'Mirassol'},
        {ddd: '018', city: 'Tupi Paulista'}
      ]);
    });
  }
}

class MockPlanCalculatorService {}
