import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { HttpService } from './providers/http/http.service';
import { PlanOutputModule } from './components/plan-output/plan-output.module';
import { DddInputModule } from './components/ddd-input/ddd-input.module';
import { PlanCalculatorService } from './components/plan-output/services/plan-calculator.service';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    PlanOutputModule,
    DddInputModule
  ],
  providers: [
    HttpService,
    PlanCalculatorService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
