import { Component, OnInit } from '@angular/core';

import { HttpService } from './providers/http/http.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  plans: any[];
  details: any[];
  origin: String;
  destiny: String;
  minutes;
  constructor(public http: HttpService) {
    this.http.planDescriptionList()
      .then(data => {
        this.plans = data;
      })
      .catch(err => console.log(err));
  }

  ngOnInit(): void {
    document.querySelector('#minutes').addEventListener('keypress', this.validateCharacter);
  }

  changeOrigin(origin) {
    this.origin = origin;
  }
  changeDestiny(destiny) {
    this.destiny = destiny;
  }
  validateCharacter(event) {
    if (event.which > 47 && event.which < 58) { // Check if typing is a valid number character (0 to 9)
      return;
    }
    event.preventDefault();
  }
}
