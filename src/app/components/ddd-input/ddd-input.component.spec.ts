import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DddInputComponent } from './ddd-input.component';
import { HttpService } from '../../providers/http/http.service';

describe('DddInputComponent', () => {
  let component: DddInputComponent;
  let fixture: ComponentFixture<DddInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DddInputComponent ],
      providers: [
        {provide: HttpService, useValue: MockHttpService}
      ]
    })
    .compileComponents();
  }));

  // beforeEach(() => {
  //   fixture = TestBed.createComponent(DddInputComponent);
  //   component = fixture.componentInstance;
  //   fixture.detectChanges();
  // });

  it('should create fixture', () => {
    fixture = TestBed.createComponent(DddInputComponent);
    expect(fixture).toBeTruthy();
  });

  it('should create', () => {
    fixture = TestBed.createComponent(DddInputComponent);
    component = fixture.componentInstance;
    expect(component).toBeTruthy();
  });
});

class MockHttpService {
  static locationDetailList() {
    return new Promise<any>((resolve, reject) => {
      resolve([
        {ddd: '011', city: 'São Paulo'},
        {ddd: '016', city: 'Ribeirão Preto'},
        {ddd: '017', city: 'Mirassol'},
        {ddd: '018', city: 'Tupi Paulista'}
      ]);
    });
  }
}
