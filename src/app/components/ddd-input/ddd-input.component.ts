import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

import { HttpService } from '../../providers/http/http.service';

@Component({
  selector: 'app-ddd-input',
  templateUrl: './ddd-input.component.html',
  styleUrls: ['./ddd-input.component.css']
})
export class DddInputComponent implements OnInit {

  @Input() title: String = '';
  @Output() onsetvalue = new EventEmitter();
  componentId: String = `ddd-input-${Date.now().toString()}`;
  locations: any[] = [];
  location = {};
  showModal = false;
  dddCode: String = '';
  constructor(public http: HttpService) {
    this.http.locationDetailList()
      .then(data => {
        this.locations = data;
      })
      .catch(err => console.log(err));
  }

  ngOnInit() {
  }

  defineDDD(location) {
    this.location = location;
    this.showModal = false;
    this.onsetvalue.emit(location.ddd);
  }

}
