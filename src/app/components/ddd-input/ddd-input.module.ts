import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { DddInputComponent } from './ddd-input.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [
    DddInputComponent
  ],
  declarations: [
    DddInputComponent
  ]
})
export class DddInputModule { }
