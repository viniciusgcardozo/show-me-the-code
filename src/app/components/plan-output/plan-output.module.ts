import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PlanOutputComponent } from './plan-output.component';

@NgModule({
  imports: [
    CommonModule
  ],
  exports: [
    PlanOutputComponent
  ],
  declarations: [
    PlanOutputComponent
  ]
})
export class PlanOutputModule { }
