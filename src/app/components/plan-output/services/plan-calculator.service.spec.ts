import { TestBed, inject } from '@angular/core/testing';

import { PlanCalculatorService } from './plan-calculator.service';
import { HttpService } from '../../../providers/http/http.service';

describe('PlanCalculatorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        PlanCalculatorService,
        {provide: HttpService, useValue: MockHttp}
      ]
    });
  });

  it('should be created', inject([PlanCalculatorService], (service: PlanCalculatorService) => {
    expect(service).toBeTruthy();
  }));

  it('should consider descount', inject([PlanCalculatorService], (service: PlanCalculatorService) => {
    service.pricing = [
      {origin: '011', destiny: '016', price: '1.90'},
      {origin: '016', destiny: '011', price: '2.90'},
      {origin: '011', destiny: '017', price: '1.70'},
      {origin: '017', destiny: '011', price: '2.70'},
      {origin: '011', destiny: '018', price: '0.90'},
      {origin: '018', destiny: '011', price: '1.90'}
    ];
    const result = service.calculate(20, '011', '016', 30);
    expect(JSON.stringify(result)).toBe(JSON.stringify(['0', '00']));
  }));

  it('should calc value', inject([PlanCalculatorService], (service: PlanCalculatorService) => {
    service.pricing = [
      {origin: '011', destiny: '016', price: '1.90'},
      {origin: '016', destiny: '011', price: '2.90'},
      {origin: '011', destiny: '017', price: '1.70'},
      {origin: '017', destiny: '011', price: '2.70'},
      {origin: '011', destiny: '018', price: '0.90'},
      {origin: '018', destiny: '011', price: '1.90'}
    ];
    const result = service.calculate(200, '018', '011', 120);
    expect(JSON.stringify(result)).toBe(JSON.stringify(['167', '20']));
  }));
});

class MockHttp {
  static locationAndPricingList() {
    return new Promise<any>((resolve, reject) => {
      resolve(JSON.parse(`
      {"total":6,"data":[
        {"origin":"011","destiny":"016","price":"1.90"},
        {"origin":"016","destiny":"011","price":"2.90"},
        {"origin":"011","destiny":"017","price":"1.70"},
        {"origin":"017","destiny":"011","price":"2.70"},
        {"origin":"011","destiny":"018","price":"0.90"},
        {"origin":"018","destiny":"011","price":"1.90"}
      ]}`));
    });
  }
}
