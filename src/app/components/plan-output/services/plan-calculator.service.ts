import { Injectable } from '@angular/core';
import { HttpService } from '../../../providers/http/http.service';

@Injectable()
export class PlanCalculatorService {

  pricing;
  constructor(public http: HttpService) {
    this.http.locationAndPricingList()
      .then(data => {
        this.pricing = data;
      })
      .catch(err => console.log(err));
  }

  calculate(minutes, origin, destiny, descount) {
    if (!this.pricing || !minutes) { // Chack if pricing map is ok
      return [];
    }

    let target = this.pricing.filter(item => item.origin === origin);
    if (!target || target.length <= 0) { // Check if have this origin into pricing map
      return [];
    }

    target = target.filter(item => item.destiny === destiny);
    if (!target || target.length <= 0) { // Check if have this destiny (for previous origin) into pricing map
      return [];
    }

    minutes = parseInt(minutes, 10);
    descount = parseInt(descount, 10);

    if (minutes <= descount) { // minutes less than discount => free call
      return ['0', '00'];
    }

    let value = (minutes - descount) * target[0].price;
    value *= descount === 0 ? 1 : 1.1;
    return value.toFixed(2).toString().split('.');
  }
}
