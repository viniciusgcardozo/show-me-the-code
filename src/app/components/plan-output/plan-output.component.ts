import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';

import { HttpService } from '../../providers/http/http.service';
import { PlanCalculatorService } from './services/plan-calculator.service';

@Component({
  selector: 'app-plan-output',
  templateUrl: './plan-output.component.html',
  styleUrls: ['./plan-output.component.css']
})
export class PlanOutputComponent implements OnChanges {

  @Input() title: String = '';
  @Input() origin: String = '';
  @Input() destiny: String = '';
  @Input() minutes: number;
  @Input() descount: number;
  result;

  constructor(public calculator: PlanCalculatorService) { }

  ngOnChanges(changes: SimpleChanges) {
    for (const key in changes) {
      if (this.hasOwnProperty(key)) {
        this[key] = changes[key].currentValue;
      }
    }
    this.calculate();
  }

  calculate() {
    this.result = this.calculator.calculate(this.minutes, this.origin, this.destiny, this.descount);
  }

}
