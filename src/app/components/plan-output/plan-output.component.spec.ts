import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';

import { PlanOutputComponent } from './plan-output.component';
import { PlanCalculatorService } from './services/plan-calculator.service';

describe('PlanOutputComponent', () => {
  let component: PlanOutputComponent;
  let fixture: ComponentFixture<PlanOutputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlanOutputComponent ],
      providers: [
        {provide: PlanCalculatorService, useValue: MockPlanCalculatorService}
      ]
    })
    .compileComponents();
  }));

  // beforeEach(() => {
  //   fixture = TestBed.createComponent(PlanOutputComponent);
  //   component = fixture.componentInstance;
  //   element = fixture.nativeElement;
  //   fixture.detectChanges();
  // });

  it('should create fixture', () => {
    fixture = TestBed.createComponent(PlanOutputComponent);
    expect(fixture).toBeTruthy();
  });

  it('should create component', () => {
    fixture = TestBed.createComponent(PlanOutputComponent);
    component = fixture.componentInstance;
    expect(component).toBeTruthy();
  });

  it('check value changing', () => {
    fixture = TestBed.createComponent(PlanOutputComponent);
    component = fixture.componentInstance;
    component.result = undefined;
    component.calculator.calculate = (minutes, origin, destiny, descount) => {
      return [];
    };
    component.calculate();
    expect(component.result !== undefined).toBe(true);
  });
});

class MockPlanCalculatorService {
  public calculate(minutes, origin, destiny, descount) {
    return [];
  }
}
